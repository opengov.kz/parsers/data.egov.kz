from itertools import cycle
import warnings
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
import csv
import os

class Dataset:
    name = str
    description = str
    page_url = str
    data_link = str
    meta_link = str
    government_agency = str
    actual_type = str
    actual_status = str
    publish_date = str
    update_date = str
    owner = str

    def __init__(self, page_url, name, description, data_link, meta_link, government_agency, actual_type, actual_status, publish_date, update_date, owner):
        self.page_url = page_url
        self.name = name
        self.description = description
        self.data_link = data_link
        self.meta_link = meta_link
        self.government_agency = government_agency
        self.actual_type = actual_type
        self.actual_status = actual_status
        self.publish_date = publish_date
        self.update_date = update_date
        self.owner = owner

    def __repr__(self):
        return str(self.__dict__)

    def __iter__(self):
        return iter([self.name, self.description, self.page_url, self.owner, self.government_agency, self.actual_type, self.actual_status, self.publish_date, self.update_date, self.data_link, self.meta_link])

    def keys(self):
        return self.__dict__.keys()

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    def __getitem__(self, key):
        return self.__dict__[key]

    def __len__(self):
        return len(self.__dict__)

    def get(self, key, default=None):
        return self.__dict__[key] if key in self.__dict__ else default

headers = {
    'authority': 'www.kith.com',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36',
    'sec-fetch-dest': 'document',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'accept-language': 'ru-RU,ru;q=0.9',
}
warnings.filterwarnings('ignore', message='Unverified HTTPS request')

session = requests.session()

filename = 'data.egov.kz.csv'

hostname = 'https://data.egov.kz'

fnames = ['page_url', 'name', 'description', 'owner', 'government_agency',
          'actual_type', 'actual_status', 'publish_date', 'update_date', 'data_link', 'meta_link']

datasets = []

def parse_dataset_page(url):
    print('parse_dataset_page')
    print(url)

    #driver = webdriver.PhantomJS()

    # if (platform.system() == 'Windows'):
    #     driver = webdriver.PhantomJS(executable_path=getcwd() + "\phantomjs")
    #
    # if (platform.system() == 'Darwin'):
    #     driver = webdriver.PhantomJS(executable_path=getcwd() + "/phantomjs")


    timeout = 3
    try:

        webdriver.DesiredCapabilities.CHROME['acceptSslCerts'] = True

        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.minimize_window()
        driver.get(url)
        element_present = EC.presence_of_element_located((By.ID, 'body-id'))
        WebDriverWait(driver, timeout).until(element_present)
    except Exception as e:
        print('Ошибка соединения')
        #proxy_pool.remove(p)
        #driver.close()
    else:
        print("Page loaded")
        # Obtain source text
        source_code = driver.page_source

        #response = session.get(url, headers=headers, verify=False)

        #soup = BeautifulSoup(response.text, 'html.parser')

        soup = BeautifulSoup(source_code, 'html.parser')

        try:
            tbody = soup.find(id="metaTab").find_next('table').find_next('tbody')
        except:
            print('Не найден metaTab')
        else:

            #print(tbody)

            name = ''
            description = ''
            owner = ''
            government_agency = ''
            actual_type = ''
            actual_status = ''
            publish_date = ''
            update_date = ''
            data_link = ''
            meta_link = ''

            for tr in tbody.find_all('tr'):
                try:
                    td = tr.find_all('td')
                    #print(td[0].text)
                    #print(td[1].text)
                    attibute_name = td[0].text.strip()
                    attribute_value = td[1].text.strip()
                    if attibute_name == 'Название':
                        name = attribute_value
                    if attibute_name == 'Описание':
                        description = attribute_value
                    if attibute_name == 'Владелец':
                        owner = attribute_value
                    if attibute_name == 'Государственный орган':
                        government_agency = attribute_value
                    if attibute_name == 'Тип актуализации':
                        actual_type = attribute_value
                    if attibute_name == 'Статус актуальности':
                        actual_status = attribute_value
                    if attibute_name == 'Дата размещения':
                        publish_date = attribute_value
                    if attibute_name == 'Дата обновления':
                        update_date = attribute_value
                    #if name == 'Ссылка на данные':
                        #print(td[1])
                        #children = td[1].findChildren("a", recursive=False)
                        #for child in children:
                        #    dataset.data_link = child.get('href')
                    #if name == 'Ссылка на мета информацию':
                        #print(td[1])
                        #children = td[1].findChildren("a", recursive=False)
                        #for child in children:
                        #    dataset.meta_link = child.get('href')
                except AttributeError as error:
                    print(format(error))

            try:
                data_link = soup.find(id="dataLink").find_next('a').get('href')
                meta_link = soup.find(id="metaLink").find_next('a').get('href')
            except Exception as error:
                print(format(error))

            dataset = Dataset(url, name, description, data_link, meta_link, government_agency, actual_type, actual_status, publish_date, update_date, owner)
            #print(dataset.__repr__())

            with open('data.egov.kz.csv', 'a+', newline='', encoding='utf-8') as f:
                # keys = datasets[0].keys()
                writer = csv.DictWriter(f, fieldnames=fnames, quotechar='"', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(dataset)

            #datasets.append(dataset)
            #break

    driver.close()

def parse_page(url):

    print('parse_page')

    print(url)

    response = session.get(url, headers=headers, verify=False)

    soup = BeautifulSoup(response.text, 'html.parser')

    for element in soup.find_all('div', class_='search-result-item'):
        #print(element)
        link = element.find_next('h4').find_next('a')

        dataset_url = hostname+link.get('href')

        parse_dataset_page(dataset_url)

    next_page_url = url

    for pagination in soup.find_all('ul', class_='pagination'):
        parse_next = False
        for li in pagination.find_all('li'):
            # print(li['class'])
            # print(parse_next)
            if parse_next:
                page_link = li.find_next('a')
                next_page_url = hostname + page_link.get('href')
                break
            else:
                if li.has_attr("class") and len(li['class']) > 0 and li['class'][0] == 'active':
                    parse_next = True
    if next_page_url != url:
        parse_page(next_page_url)

if os.path.exists(filename):
    os.remove(filename)

with open(filename, 'w', newline='') as f:
    writer = csv.DictWriter(f, fieldnames=fnames, quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writeheader()

parse_page(hostname+'/datasets/search?page=1')

#print(get_proxies)

